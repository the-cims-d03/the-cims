from urllib import response
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from .forms import *
from django.views.decorators.csrf import csrf_exempt


def read_misi_utama(request):
    if request.session.has_key('username'):
        response = {}
        response['role'] = [request.session['username'][2]]
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM MISI")
            response['daftar_misi_utama'] = cursor.fetchall()

            cursor.execute("SELECT nama_misi FROM MENJALANKAN_MISI_UTAMA")
            response['daftar_menjalankan_misi_utama'] = cursor.fetchall()
            
            daftar_misi_utama = []
            for misi_utama in response['daftar_menjalankan_misi_utama']:
                daftar_misi_utama.append(misi_utama[0])

            response['daftar_menjalankan_misi_utama'] = daftar_misi_utama
            
        return render(request, 'read_misi_utama.html', response)
    else:
        return HttpResponseRedirect('/login')

def read_detail_misi_utama(request):
    nama = request.GET.get("nama", "")

    if request.session.has_key('username'):
        response = {}
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute(f"""
                SELECT * FROM MISI
                WHERE nama = '{nama}'
            """)
            response['detail_misi_utama'] = cursor.fetchall()
        return render(request, 'detail_misi_utama.html', response)
    else:
        return HttpResponseRedirect('/login')

# Admin Only
@csrf_exempt
def create_misi(request):
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            if (request.method == "POST"):
                form = CreateMisiUtamaForm(request.POST)
                if form.is_valid():
                    nama = request.POST['nama']
                    efek_energi = request.POST['efek_energi']
                    efek_hubungan_sosial = request.POST['efek_hubungan_sosial']
                    efek_kelaparan = request.POST['efek_kelaparan']
                    syarat_energi = request.POST['syarat_energi']
                    syarat_hubungan_sosial = request.POST['syarat_hubungan_sosial']
                    syarat_kelaparan = request.POST['syarat_kelaparan']
                    completion_time = request.POST['completion_time']
                    reward_koin = request.POST['reward_koin']
                    reward_xp = request.POST['reward_xp']
                    deskripsi = request.POST['deskripsi']

                    with connection.cursor() as cursor:
                        cursor.execute("SET SEARCH_PATH TO THECIMS")
                        cursor.execute("INSERT INTO MISI VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", [nama, efek_energi, efek_hubungan_sosial, efek_kelaparan, syarat_energi, syarat_hubungan_sosial, syarat_kelaparan, completion_time, reward_koin, reward_xp, deskripsi])
                        cursor.execute("INSERT INTO MISI_UTAMA VALUES(%s)", [nama])
                    return redirect('/misi-utama/')
                else:
                    return HttpResponse("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu", status=400)

            form = CreateMisiUtamaForm()
            response = {}
            response["misi_form"] = form
            return render(request, 'create_misi.html', response)
    else:
        return HttpResponseRedirect('/login')

# Admin Only
@csrf_exempt
def delete_misi_utama(request):
    nama = request.GET.get("nama", "")
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("DELETE FROM MISI WHERE nama = %s", [nama])
                cursor.execute("DELETE FROM MISI_UTAMA WHERE nama_misi = %s", [nama])
            return redirect('/misi-utama/')
    return redirect('/login')
