from django.urls import path
from .views import *

app_name = 'misi_utama'
urlpatterns = [
    path('', read_misi_utama, name='read_misi_utama'),
    path('create/', create_misi, name='create_misi'),
    path('detail/', read_detail_misi_utama, name='read_detail_misi_utama'),
    path('delete/', delete_misi_utama, name='delete_misi_utama')
]
