from django import forms

class CreateMisiUtamaForm(forms.Form):
    nama = forms.CharField(label='nama', widget=forms.TextInput(
        attrs={'id': 'nama', 'placeholder': 'Masukkan nama misi utama'}))
    efek_energi = forms.IntegerField(label='efek_energi', widget=forms.NumberInput(
        attrs={'id': 'efek_energi', 'placeholder': 'Masukkan nilai efek energi'}))
    efek_hubungan_sosial = forms.IntegerField(label='efek_hubungan_sosial', widget=forms.NumberInput(
        attrs={'id': 'efek_hubungan_sosial', 'placeholder': 'Masukkan nilai efek hubungan sosial'}))
    efek_kelaparan = forms.IntegerField(label='efek_kelaparan', widget=forms.NumberInput(
        attrs={'id': 'efek_kelaparan', 'placeholder': 'Masukkan nilai efek kelaparan'}))
    syarat_energi = forms.IntegerField(label='syarat_energi', widget=forms.NumberInput(
        attrs={'id': 'syarat_energi', 'placeholder': 'Masukkan nilai syarat energi'}))
    syarat_hubungan_sosial = forms.IntegerField(label='syarat_hubungan_sosial', widget=forms.NumberInput(
        attrs={'id': 'syarat_hubungan_sosial', 'placeholder': 'Masukkan nilai syarat hubungan sosial'}))
    syarat_kelaparan = forms.IntegerField(label='syarat_kelaparan', widget=forms.NumberInput(
        attrs={'id': 'syarat_kelaparan', 'placeholder': 'Masukkan nilai syarat kelaparan'}))
    completion_time = forms.TimeField(label='nama', widget=forms.TimeInput(
        attrs={'id': 'completion_time', 'placeholder': 'Masukkan completion time'}))
    reward_koin = forms.IntegerField(label='reward_koin', widget=forms.NumberInput(
        attrs={'id': 'reward_koin', 'placeholder': 'Masukkan nilai reward koin'}))
    reward_xp = forms.IntegerField(label='reward_xp', widget=forms.NumberInput(
        attrs={'id': 'reward_xp', 'placeholder': 'Masukkan nilai reward XP'}))
    deskripsi = forms.CharField(label='deskripsi', widget=forms.Textarea(
        attrs={'id': 'deskripsi', 'placeholder': 'Masukkan deskripsi'}))
