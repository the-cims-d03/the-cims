from django.urls import path

from .views import *

app_name = 'tokoh'

urlpatterns = [
    path('', index, name='index'),
    path('create',buat_tokoh,name='buat_tokoh'),
    path('detail/<str:name>/<str:nama>',detail_tokoh,name='detail_tokoh'),
    path('detail/<str:name>/<str:nama>/update',update_tokoh,name='update_tokoh'),

]