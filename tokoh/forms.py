from django import forms

class TokohForm(forms.Form):
    nama = forms.CharField(label='nama', max_length=50)
    jenis_kelamin = forms.CharField(label='jenis_kelamin', max_length=10)
    warna_kulit = forms.CharField(label='warna_kulit', max_length=7)
    pekerjaan = forms.CharField(label='pekerjaan',max_length=20)

class UpdateTokohForm(forms.Form):
    id_rambut = forms.CharField(label='id_rambut',max_length=5)
    id_mata = forms.CharField(label='id_mata',max_length=5)    
    id_rumah = forms.CharField(label='id_rumah',max_length=5)    