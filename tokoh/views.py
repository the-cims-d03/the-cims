from pprint import pprint
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from .forms import *
# Create your views here.


# Function to return every row of data from query
def fetch_to_str(cursor):
    desc = cursor.fetchall()
    nt_result = tuple([col[0] for col in desc])
    return nt_result

def index(request):
    cursor = connection.cursor()
    result = []
    try:
        if request.session.has_key('username'):
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            role = request.session['username'][2]
            if role == 'Admin':
                cursor.execute("SELECT * FROM TOKOH")
                result = cursor.fetchall()
            else:
                name = request.session['username'][0]
                print("NAMANYA " + name)
                cursor.execute(f"SELECT * FROM TOKOH WHERE username_pengguna = '{name}';")
                result = cursor.fetchall()
                print(result)
            return render(request, 'read_tokoh.html',{'result' : result})
        else:
            return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()

def detail_tokoh(request,nama,name):
    cursor = connection.cursor()
    result = []
    try:
        if request.session.has_key('username'):
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute(
                f'''
                SELECT nama,id_rambut,id_mata,id_rumah,warna_kulit,pekerjaan
                FROM TOKOH
                WHERE username_pengguna = '{nama}'
                AND nama = '{name}'
                '''
                )
            result = cursor.fetchone()
            return render(request,'detail_tokoh.html',{'result' : result})
        else:
            return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()

def buat_tokoh(request):
    cursor = connection.cursor()
    try:
        if request.method == 'POST':
            if request.session['username'][2] == "Pemain":
                form = TokohForm(request.POST)
                if form.is_valid():
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute(f'''
                        INSERT INTO TOKOH(username_pengguna,nama,jenis_kelamin,warna_kulit,pekerjaan)
                        VALUES(
                            '{request.session['username'][0]}',
                            '{form.cleaned_data['nama']}',
                            '{form.cleaned_data['jenis_kelamin']}',
                            '{form.cleaned_data['warna_kulit']}',
                            '{form.cleaned_data['pekerjaan']}'
                        )
                    ''')
                    return HttpResponseRedirect("/tokoh")
        else:
            if request.session.has_key('username'):
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(
                    f'''
                    SELECT kode
                    FROM WARNA_KULIT
                    '''
                    )
                warna_kulit = fetch_to_str(cursor)
                print(warna_kulit)
                cursor.execute(
                    f'''
                    SELECT nama
                    FROM PEKERJAAN
                    '''
                    )
                nama_pekerjaan = fetch_to_str(cursor)
                return render(request,'buat_tokoh.html',{'warna_kulit' : warna_kulit, 'nama_pekerjaan' : nama_pekerjaan})
            else:
                return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()    

def update_tokoh(request,nama,name):
    cursor = connection.cursor()
    try:
        if request.method == 'POST':
            if request.session['username'][2] == "Pemain":
                print("ey")
                form = UpdateTokohForm(request.POST)
                print(form.errors)
                if form.is_valid():
                    print("YOWS")
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute(f'''
                        UPDATE TOKOH
                        SET id_rambut = '{form.cleaned_data['id_rambut']}',
                        id_mata = '{form.cleaned_data['id_mata']}',
                        id_rumah = '{form.cleaned_data['id_rumah']}'
                        WHERE username_pengguna = '{nama}'
                        AND nama = '{name}';
                    ''')
                    return HttpResponseRedirect(f"/tokoh/detail/{name}/{nama}")
        else:
            print("SINI")
            if request.session.has_key('username'):
                print("hmm")
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(
                    f'''
                    SELECT id_koleksi
                    FROM RAMBUT
                    '''
                    )
                id_rambut = fetch_to_str(cursor)
                cursor.execute(
                    f'''
                    SELECT id_koleksi
                    FROM MATA
                    '''
                    )
                id_mata = fetch_to_str(cursor)
                cursor.execute(
                    f'''
                    SELECT id_koleksi
                    FROM RUMAH
                    '''
                    )
                id_rumah = fetch_to_str(cursor)
                return render(request,'update_tokoh.html',{'id_rambut' : id_rambut, 'id_mata' : id_mata,'id_rumah' : id_rumah,'nama' :name})
            else:
                return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()    
