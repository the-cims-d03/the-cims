from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from .forms import *
from datetime import datetime
# Create your views here.
def fetch_to_str(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.fetchall()
    nt_result = tuple([col[0] for col in desc])
    return nt_result    


def index(request):
    cursor = connection.cursor()
    result = []
    try:
        if request.session.has_key('username'):
            role = request.session['username'][2]
            nama = request.session['username'][0]
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            print("ROLENYA " + role)
            if role == 'Pemain': 
                cursor.execute(f'''
                SELECT m.nama_tokoh, k.nama,m.waktu
                FROM MENGGUNAKAN_BARANG m,KOLEKSI_JUAL_BELI k WHERE m.username_pengguna = '{nama}'
                AND m.id_barang = k.id_koleksi;
                
                ''')
                result = cursor.fetchall()
                return render(request, 'read_menggunakan_barang.html',{'result' : result})
            else:
                cursor.execute(f'''
                SELECT m.username_pengguna,m.nama_tokoh, k.nama,m.waktu
                FROM MENGGUNAKAN_BARANG m,KOLEKSI_JUAL_BELI k WHERE m.id_barang = k.id_koleksi;
                ''')
                result = cursor.fetchall()
                return render(request, 'read_menggunakan_barang.html',{'result' : result})
        else:
            return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()


def create_menggunakan_barang(request):
    cursor = connection.cursor()
    try:
        if request.method == "POST":
            nama = request.session['username'][0]
            form = UseBarangForm(request.POST)
            if form.is_valid():
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(f"SELECT tingkat_energi FROM BARANG WHERE id_koleksi = '{form.cleaned_data['id_koleksi']}'")
                tingkat_energi = int(cursor.fetchone()[0])
                cursor.execute(f"SELECT energi FROM TOKOH WHERE username_pengguna = '{nama}' AND nama = '{form.cleaned_data['nama']}'")
                energi_tokoh = int(cursor.fetchone()[0])
                if energi_tokoh >= tingkat_energi:
                    cursor.execute(f'''
                        INSERT INTO MENGGUNAKAN_BARANG VALUES
                        (
                            '{nama}',
                            '{form.cleaned_data['nama']}',
                            '{datetime.now()}',
                            '{form.cleaned_data['id_koleksi']}'
                            );
                    ''')
                    return HttpResponseRedirect('/use_barang')
                else:
                    errors = "Tingkat energi anda tidak mencukupi untuk menggunakan barang ini!"
                    cursor.execute(f"SELECT nama FROM TOKOH WHERE username_pengguna = '{nama}'")
                    nama_tokoh = fetch_to_str(cursor)
                    cursor.execute(f"SELECT k.id_koleksi FROM KOLEKSI_TOKOH k,BARANG b WHERE username_pengguna = '{nama}' AND k.id_koleksi = b.id_koleksi")
                    koleksi_barang = fetch_to_str(cursor)
                    return render(request, 'create_menggunakan_barang.html',{'nama_tokoh' : nama_tokoh,'koleksi_barang' : koleksi_barang,'errors' : errors})
                

        else:
            if request.session.has_key('username'):
                role = request.session['username'][2]
                nama = request.session['username'][0]
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                print("ROLENYA " + role)
            if role == 'Pemain': 
                cursor.execute(f"SELECT  nama FROM TOKOH WHERE username_pengguna = '{nama}'")
                nama_tokoh = fetch_to_str(cursor)
                cursor.execute(f"SELECT k.id_koleksi FROM KOLEKSI_TOKOH k,BARANG b WHERE username_pengguna = '{nama}' AND k.id_koleksi = b.id_koleksi")
                koleksi_barang = fetch_to_str(cursor)
                return render(request, 'create_menggunakan_barang.html',{'nama_tokoh' : nama_tokoh,'koleksi_barang' : koleksi_barang})
            else:
                return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()

