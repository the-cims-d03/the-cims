from django.urls import path

from .views import *

app_name = 'use_barang'

urlpatterns = [
    path('', index, name='index'),
    path('create',create_menggunakan_barang,name="create_use_barang")
]