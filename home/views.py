from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from .forms import *

# Create your views here.

# Function to return every row of data from query
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request):
    cursor = connection.cursor()
    result = []
    try:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT EMAIL FROM PEMAIN")
        result = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'home/index.html', {'result': result})

def login(request):

    result = []

    # Define login form
    MyLoginForm = LoginForm(request.POST)

    # Form submission
    if (MyLoginForm.is_valid() and request.method == 'POST'):
        # Get data from form
        username = MyLoginForm.cleaned_data['username']
        password = MyLoginForm.cleaned_data['password']

        # Run SQL QUERY
        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM \
                           (SELECT * FROM PEMAIN UNION SELECT username, Null as email, password, Null as no_hp, Null as koin FROM ADMIN) AS P \
                           WHERE username ='" + username + "' AND PASSWORD = '" + password + "'")
            result = cursor.fetchone()
            print(result[1])
            if result[1] == None:
                role = "Admin"
            else:
                role = "Pemain"

            if(result == None): 
                return HttpResponseNotFound("The user does not exist")

            # Redirect the cursor towards public so it can access Django basic features
            cursor.execute("SET SEARCH_PATH TO public")
            request.session['username'] = [username, password, role, result]

        except Exception as e:
            print(e)

        finally:
            # Don't forget to close
            cursor.close()

        return HttpResponseRedirect('/homepage')

    else:
        MyLoginForm = LoginForm()

    return render(request, 'home/login.html', {'form' : MyLoginForm})

# Function to test logged in result
def loggedInView(request):
    if request.session.has_key('username'):
        cursor = connection.cursor()
        result = []
        try:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM \
                           (SELECT * FROM PEMAIN UNION SELECT username, Null as email, password, Null as no_hp, Null as koin FROM ADMIN) AS P \
                           WHERE USERNAME = '"+ request.session['username'][0] +"'")
            result = namedtuplefetchall(cursor)
            role = request.session['username'][2]
            print(role)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        
        if role == "Admin":
            return render(request, 'home/home_admin.html', {"result" : result, "role" : role})
        else:
            return render(request, 'home/home_pemain.html', {"result" : result, "role" : role})
    else:
        return HttpResponseRedirect('/login')
    
# Function to log out the user
def logout(request):
   try:
        del request.session['username']
   except:
        pass
   return HttpResponseRedirect('/login')

