from django.shortcuts import render
from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.db import connection
from collections import namedtuple
from home.views import namedtuplefetchall
from .forms import *

# Create your views here.
def listKategoriApparel(request):
    #Check Session
    if request.session.has_key('username'):
        result = []
        role = request.session['username'][2]
        # Run SQL QUERY
        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM KATEGORI_APPAREL")
            result = namedtuplefetchall(cursor)
            print(result)

        except Exception as e:
            print(e)

        finally:
            # Don't forget to close
            cursor.close()

        return render(request, 'listKategoriApparel.html', {'result' : result, 'role' : role})
        
    else:
        return HttpResponseRedirect('/login')
    
def addKategoriApparel(request):
    if request.session.has_key('username'):
        
        if not request.session['username'][2] == 'Admin':
            return HttpResponseRedirect('/koleksi/kategori-apparel')
        
        # Define form
        MyApparelForm = KategoriApparelForm(request.POST)

        # Form submission
        if (MyApparelForm.is_valid() and request.method == 'POST'):
            # Get data from form
            nama_kategori = MyApparelForm.cleaned_data['nama_kategori']
            
            # Run SQL QUERY
            try:
                cursor = connection.cursor()
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("INSERT INTO kategori_apparel VALUES ('" + nama_kategori + "')")

            except Exception as e:
                print(e)

            finally:
                # Don't forget to close
                cursor.close()

            return HttpResponseRedirect('/koleksi/kategori-apparel')

        else:
            MyApparelForm = KategoriApparelForm()

        return render(request, 'addKategoriApparel.html', {'form' : MyApparelForm})
        
    else:
        return HttpResponseRedirect('/login')
    
def listKoleksi(request):
    return render(request, 'listKoleksiSelector.html')

def addKoleksi(request):
    return render(request, 'addKoleksiSelector.html')

def listKoleksiA(request, jenisKoleksi):
    if not request.session.has_key('username'):
        return HttpResponseRedirect('/login')
    
    result = []
    role = request.session['username'][2]
    if jenisKoleksi == 'rumah':
        p = 'kapasitas_barang'
    else:
        p = 'tingkat_energi'
    # Run SQL QUERY
    try:
        cursor = connection.cursor()
        if jenisKoleksi == 'rumah' or jenisKoleksi == 'barang':
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT id, nama, harga, harga_beli, " + p + " \
                            FROM " + jenisKoleksi + " r, koleksi k, koleksi_jual_beli kj \
                            WHERE r.id_koleksi = k.id \
                            AND kj.id_koleksi = k.id")
            
        elif jenisKoleksi == 'apparel':
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT id, nama, harga, harga_beli, " + p + " \
                            FROM " + jenisKoleksi + " r, koleksi k, koleksi_jual_beli kj\
                            WHERE r.id_koleksi = k.id \
                            AND kj.id_koleksi = k.id")
        else:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM " + jenisKoleksi + " r, koleksi k \
                            WHERE r.id_koleksi = k.id")
        result = namedtuplefetchall(cursor)
        print(result)

    except Exception as e:
        print(e)

    finally:
        # Don't forget to close
        cursor.close()

    return render(request, 'listKoleksi.html', {'result' : result, 'role' : role, 'jenisKoleksi' : jenisKoleksi})
    
def addKoleksiRambut(request):
    if not request.session.has_key('username'):
        return HttpResponseRedirect('/login')    
        
    if not request.session['username'][2] == 'Admin':
        return HttpResponseRedirect('/homepage')
    
    # Define form
    MyKoleksiForm = KoleksiRambutForm(request.POST)

    # Form submission
    if (MyKoleksiForm.is_valid() and request.method == 'POST'):
        # Get data from form
        id = MyKoleksiForm.cleaned_data['id']
        harga_jual = MyKoleksiForm.cleaned_data['harga_jual']
        tipe_rambut = MyKoleksiForm.cleaned_data['tipe_rambut']
        
        # Run SQL QUERY
        try:
            cursor = connection.cursor()
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("INSERT INTO KOLEKSI VALUES ('" + id + "', " + harga_jual + ")")
            cursor.execute("INSERT INTO RAMBUT VALUES ('" + id + "', " + tipe_rambut + ")")

        except Exception as e:
            print(e)

        finally:
            # Don't forget to close
            cursor.close()

        return HttpResponseRedirect('/koleksi/rambut')

    else:
        MyKoleksiForm = KoleksiRambutForm()

    return render(request, 'addKoleksiRambut.html', {'form' : MyKoleksiForm})
    
def listKoleksiTokoh(request):
    if not request.session.has_key('username'):
        return HttpResponseRedirect('/login')
    
    result = []
    role = request.session['username'][2]
    print(request.session['username'][0])
    # Run SQL QUERY
    try:
        cursor = connection.cursor()
        if role == 'Pemain':
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM koleksi_tokoh \
                            WHERE username_pengguna = '" + request.session['username'][0] + "'")
        else:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM koleksi_tokoh")
        result = namedtuplefetchall(cursor)
        
        print(result)

    except Exception as e:
        print(e)

    finally:
        # Don't forget to close
        cursor.close()

    return render(request, 'listKoleksiTokoh.html', {'result' : result, 'role' : role})

    
        
    