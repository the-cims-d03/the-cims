from django.urls import path
from .views import *

app_name = 'koleksi'
urlpatterns = [
    path('kategori-apparel', listKategoriApparel, name='kategori-apparel'),
    path('kategori-apparel/add', addKategoriApparel, name='add-kategori-apparel'),
    path('', listKoleksi, name='list-koleksi'),
    path('list/<jenisKoleksi>', listKoleksiA, name='koleksi-rambut'),
    path('add', addKoleksi, name='add-koleksi'),
    path('add/rambut', addKoleksiRambut, name='add-koleksi-rambut'),
    path('koleksi-tokoh', listKoleksiTokoh, name='list-koleksi-tokoh'),
]
