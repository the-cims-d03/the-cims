from django import forms
from django.db import models
from django.db.models import Max


class KategoriApparelForm(forms.Form):
    nama_kategori = forms.CharField(label='Nama kategori', max_length=50)
    
class KoleksiRambutForm(forms.Form):
    id = models.CharField()
    harga_jual = forms.IntegerField(label='Harga Jual')
    tipe_rambut = forms.CharField(label='Tipe Rambut', max_length=10)

    # def save(self, **kwargs):
    #     if not self.id:
    #         max = KoleksiRambutForm.objects.aggregate(id_max=Max('id'))['id_max']
    #         self.id = "{}{:05d}".format('RB', max if max is not None else 1)
    #     super().save(*kwargs)