from urllib import response
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from .forms import *
from django.views.decorators.csrf import csrf_exempt


def read_makanan(request):
    if request.session.has_key('username'):
        response = {}
        response['role'] = [request.session['username'][2]]
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM MAKANAN")
            response['daftar_makanan'] = cursor.fetchall()

            cursor.execute("SELECT nama_makanan DISTINC FROM MAKAN")
            response['daftar_makan'] = cursor.fetchall()

            daftar_makan = []
            for nama_makanan in response['daftar_makan']:
                daftar_makan.append(nama_makanan[0])

            response['daftar_makan'] = daftar_makan

        return render(request, 'read_makanan.html', response)
    else:
        return HttpResponseRedirect('/login')

# Admin Only
@csrf_exempt
def create_makanan(request):
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            if (request.method == "POST"):
                form = CreateMakananForm(request.POST)
                if form.is_valid():
                    nama = request.POST['nama']
                    harga = request.POST['harga']
                    tingkat_energi = request.POST['tingkat_energi']
                    tingkat_kelaparan = request.POST['tingkat_kelaparan']

                    with connection.cursor() as cursor:
                        cursor.execute("SET SEARCH_PATH TO THECIMS")
                        cursor.execute("INSERT INTO MAKANAN VALUES(%s, %s, %s, %s)", [
                                       nama, harga, tingkat_energi, tingkat_kelaparan])
                    return redirect('/makanan/')
                else:
                    return HttpResponse("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu", status=400)
                    
            form = CreateMakananForm()
            response = {}
            response["makanan_form"] = form
            return render(request, 'create_makanan.html', response)
    else:
        return HttpResponseRedirect('/login')

# Admin Only
@csrf_exempt
def update_makanan(request):
    nama = request.GET.get("nama", "")
    data = {}
    response = {}

    if (request.method == "POST"):
        form = UpdateMakananForm(request.POST)
        if (form.is_valid()):
            harga = request.POST['harga']
            tingkat_energi = request.POST['tingkat_energi']
            tingkat_kelaparan = request.POST['tingkat_kelaparan']
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(
                    "UPDATE MAKANAN SET harga = %s, tingkat_energi = %s, tingkat_kelaparan = %s WHERE nama = %s", [harga, tingkat_energi, tingkat_kelaparan, nama])
                
            return redirect('/makanan')
        else:
            return HttpResponse("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu", status=400)

    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM MAKANAN WHERE nama = %s", [nama])
        makanan_data = cursor.fetchone()

    data['nama'] = makanan_data[0]
    data['harga'] = makanan_data[1]
    data['tingkat_energi'] = makanan_data[2]
    data['tingkat_kelaparan'] = makanan_data[3]

    form = UpdateMakananForm(data)
    response['makanan_form'] = form

    return render(request, 'update_makanan.html', response)

# Admin Only
@csrf_exempt
def delete_makanan(request):
    nama = request.GET.get("nama", "")
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("DELETE FROM MAKANAN WHERE nama = %s", [nama])
    return redirect('/makanan/')

