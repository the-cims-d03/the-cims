from django import forms

class CreateMakananForm(forms.Form):
    nama = forms.CharField(label='nama', widget=forms.TextInput(
        attrs={'id': 'nama', 'placeholder': 'Masukkan nama makanan'}))
    harga = forms.IntegerField(label='harga', widget=forms.NumberInput(
        attrs={'id': 'harga', 'placeholder': 'Masukkan harga makanan'}))
    tingkat_energi = forms.IntegerField(label='tingkat_energi', widget=forms.NumberInput(
        attrs={'id': 'tingkat_energi', 'placeholder': 'Masukkan nilai tingkat energi'}))
    tingkat_kelaparan = forms.IntegerField(label='tingkat_kelaparan', widget=forms.NumberInput(
        attrs={'id': 'tingkat_kelaparan', 'placeholder': 'Masukkan nilai tingkat kelaparan'}))


class UpdateMakananForm(forms.Form):
    nama = forms.CharField(label='nama', widget=forms.TextInput(
        attrs={'id': 'nama', 'placeholder': 'nama', 'readonly': True}))
    harga = forms.IntegerField(label='harga', widget=forms.NumberInput(
        attrs={'id': 'harga', 'placeholder': 'Masukkan harga makanan'}))
    tingkat_energi = forms.IntegerField(label='tingkat_energi', widget=forms.NumberInput(
        attrs={'id': 'tingkat_energi', 'placeholder': 'Masukkan nilai tingkat energi'}))
    tingkat_kelaparan = forms.IntegerField(label='tingkat_kelaparan', widget=forms.NumberInput(
        attrs={'id': 'tingkat_kelaparan', 'placeholder': 'Masukkan nilai tingkat kelaparan'}))
