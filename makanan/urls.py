from django.urls import path
from .views import *

app_name = 'makanan'
urlpatterns = [
    path('', read_makanan, name='read_makanan'),
    path('create', create_makanan,
         name='create_makanan'),
    path('update/', update_makanan,
         name='update_makanan'),
    path('delete/', delete_makanan, name='delete_makanan')
]
