from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from .forms import *

# role: Admin & Pemain
def read_warna_kulit(request):
    response = {}
    if request.session.has_key('username'):
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM WARNA_KULIT")
            response['daftar_warna_kulit'] = cursor.fetchall()
            cursor.execute("SELECT * FROM WARNA_KULIT WK WHERE WK.kode NOT IN (SELECT T.warna_kulit FROM TOKOH T)")
            response['daftar_warna_kulit_deletable'] = cursor.fetchall()       
            response['role'] = request.session['username'][2]
        return render(request, 'read_warna_kulit.html', response)
    return redirect('/login')

# role: Admin
def create_warna_kulit(request):
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            if (request.method == "POST"):
                form = CreateWarnaKulitForm(request.POST)
                if form.is_valid():
                    kode = request.POST['kode']
                    if check_kode_availability(kode) == 0:
                        with connection.cursor() as cursor:
                            cursor.execute("SET SEARCH_PATH TO THECIMS")
                            cursor.execute("INSERT INTO WARNA_KULIT VALUES(%s)", [kode])
                        return redirect('/warna-kulit')
                    else:
                        if check_kode_availability(kode) == 1:
                            form.add_error('kode', "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu")
                        elif check_kode_availability(kode) == 2:
                            form.add_error('kode', "Kode ini sudah tercatat")
                        response["create_warna_kulit_form"] = form
                        return render(request, "create_warna_kulit.html", response)
            form = CreateWarnaKulitForm()
            response["create_warna_kulit_form"] = form
            return render(request, "create_warna_kulit.html", response)
    return redirect('/login')

# role: Admin
def delete_warna_kulit(request, kode):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("DELETE FROM WARNA_KULIT WHERE kode = %s", ['#' + kode])
    return redirect('/warna-kulit')

def check_kode_availability(kode):
    result = 0
    if len(kode) != 7 or kode[0:1] != "#":
        result = 1
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM WARNA_KULIT WHERE kode = %s", [kode])
        kode_row = cursor.fetchone()
        if kode_row != None:
            result = 2
    return result