from django.urls import path
from .views import *

app_name = 'warna_kulit'
urlpatterns = [
    path('', read_warna_kulit, name='read_warna_kulit'),
    path('create', create_warna_kulit, name='create_warna_kulit'),
    path('<kode>/delete', delete_warna_kulit, name='delete_warna_kulit')
]