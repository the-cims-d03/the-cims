from django.urls import path
from .views import *

app_name = 'menjalankan_misi_utama'
urlpatterns = [
    path('', read_menjalankan_misi_utama, name='read_menjalankan_misi_utama'),
    path('create', create_menjalankan_misi_utama,
         name='create_menjalankan_misi_utama'),
    path('update/', update_menjalankan_misi_utama,
         name='update_menjalankan_misi_utama'),
]
