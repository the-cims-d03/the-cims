from django import forms
from django.db import connection
    

class UpdateMenjalankanMisiUtamaForm(forms.Form):
    nama_tokoh = forms.CharField(label='Nama Tokoh', widget=forms.TextInput(
        attrs={'id': 'nama_tokoh', 'placeholder': 'Pilih nama tokoh', 'readonly': True}))
    nama_misi_utama = forms.CharField(label='Nama Misi Utama', widget=forms.TextInput(
        attrs={'id': 'nama_misi_utama', 'placeholder': 'Pilih misi utama', 'readonly': True}))
    status = forms.CharField(label='Status', widget=forms.TextInput(
        attrs={'id': 'status', 'placeholder': 'Masukkan status'}))
