from urllib import response
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from .forms import *
from django.views.decorators.csrf import csrf_exempt


def read_menjalankan_misi_utama(request):
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            response = {}
            response['role'] = request.session['username'][2]
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM MENJALANKAN_MISI_UTAMA")
                response['daftar_menjalankan_misi_utama'] = cursor.fetchall()
            return render(request, 'read_menjalankan_misi_utama.html', response)
        elif request.session['username'][2] == "Pemain":
            username = request.session['username'][0]
            response = {}
            response['role'] = request.session['username'][2]
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(
                    "SELECT * FROM MENJALANKAN_MISI_UTAMA WHERE username_pengguna = %s", [username])
                response['daftar_menjalankan_misi_utama'] = cursor.fetchall()
            return render(request, 'read_menjalankan_misi_utama.html', response)
    else:
        return HttpResponseRedirect('/login')

# Pemain Only
@csrf_exempt
def create_menjalankan_misi_utama(request):
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Pemain":
            username = request.session['username'][0]
            if (request.method == "POST"):
                body = request.POST
                nama_tokoh = body.get('nama_tokoh', '')
                nama_misi_utama = body.get('nama_misi_utama', '')

                if not body or not (nama_tokoh and nama_misi_utama):
                    return HttpResponse("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu", status=400)

                with connection.cursor() as cursor:
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute(
                        "SELECT * FROM MISI WHERE nama = %s", [nama_misi_utama])
                    response['data_misi_utama'] = cursor.fetchall()

                with connection.cursor() as cursor:
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute(
                        "SELECT * FROM TOKOH WHERE username_pengguna = %s AND nama = %s", [username, nama_tokoh])
                    response['data_tokoh'] = cursor.fetchall()

                if(response['data_tokoh'][0][5] >= response['data_misi_utama'][0][4] and response['data_tokoh'][0][7] >= response['data_misi_utama'][0][5] and response['data_tokoh'][0][6] <= response['data_misi_utama'][0][6]):
                    with connection.cursor() as cursor:
                        cursor.execute("SET SEARCH_PATH TO THECIMS")
                        cursor.execute("INSERT INTO MENJALANKAN_MISI_UTAMA VALUES(%s, %s, %s, %s)", [
                            username, nama_tokoh, nama_misi_utama, 'In Progress'])
                        return redirect('/menjalankan-misi-utama/')
                else:
                    return HttpResponse("Syarat misi utama tidak mencukupi sehingga misi utama tidak dapat dijalankan", status=400)

            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(f"""
                    SELECT nama FROM TOKOH
                    WHERE username_pengguna = '{username}'
                """)
                response['daftar_nama_tokoh'] = cursor.fetchall()

            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM MISI_UTAMA")
                response['daftar_misi_utama'] = cursor.fetchall()

            daftar_nama_tokoh = [('Pilih tokoh', ''), ] + \
                response['daftar_nama_tokoh']
            daftar_nama_misi_utama = [
                ('Pilih misi utama', ''), ] + response['daftar_misi_utama']

            response['daftar_nama_tokoh'] = daftar_nama_tokoh
            response['daftar_misi_utama'] = daftar_nama_misi_utama

            return render(request, 'create_menjalankan_misi_utama.html', response)
    else:
        return HttpResponseRedirect('/login')

# Pemain Only
@csrf_exempt
def update_menjalankan_misi_utama(request):
    nama_tokoh = request.GET.get("nama_tokoh", "")
    nama_misi = request.GET.get("nama_misi", "")
    data = {}
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Pemain":
            username = request.session['username'][0]
            if (request.method == "POST"):
                form = UpdateMenjalankanMisiUtamaForm(request.POST)
                if (form.is_valid()):
                    status = request.POST['status']
                    with connection.cursor() as cursor:
                        cursor.execute("SET SEARCH_PATH TO THECIMS")
                        cursor.execute(
                            "UPDATE MENJALANKAN_MISI_UTAMA SET status = %s WHERE username_pengguna = %s AND nama_tokoh = %s AND nama_misi = %s", [status, username, nama_tokoh, nama_misi])
                    return redirect('/menjalankan-misi-utama/')

            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM MENJALANKAN_MISI_UTAMA WHERE username_pengguna = %s AND nama_tokoh = %s AND nama_misi = %s", [
                               username, nama_tokoh, nama_misi])
                misi_data = cursor.fetchone()

            data['nama_tokoh'] = misi_data[1]
            data['nama_misi_utama'] = misi_data[2]
            data['status'] = misi_data[3]

            form = UpdateMenjalankanMisiUtamaForm(data)
            response['menjalankan_misi_utama_form'] = form

    return render(request, 'update_menjalankan_misi_utama.html', response)
