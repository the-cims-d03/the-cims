from django.urls import path
from .views import *

app_name = 'menggunakan_apparel'
urlpatterns = [
    path('', read_menggunakan_apparel, name='read_menggunakan_apparel'),
    path('create', create_menggunakan_apparel_1, name='create_menggunakan_apparel_1'),
    path('<nama_tokoh>/create', create_menggunakan_apparel_2, name = 'create_menggunakan_apparel_2'),
    path('<username>/<nama>/<id>/delete', delete_menggunakan_apparel, name='delete_menggunakan_apparel')
]