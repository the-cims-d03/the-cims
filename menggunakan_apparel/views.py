from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect

# role: Admin & Pemain
def read_menggunakan_apparel(request):
    response = {}
    if request.session.has_key('username'):
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            if request.session['username'][2] == "Admin":
                cursor.execute("SELECT MA.username_pengguna, MA.nama_tokoh, KJB.nama, A.warna_apparel, A.nama_pekerjaan, A.kategori_apparel, MA.id_koleksi FROM MENGGUNAKAN_APPAREL MA, KOLEKSI_JUAL_BELI KJB, APPAREL A WHERE MA.id_koleksi = KJB.id_koleksi AND KJB.id_koleksi = A.id_koleksi")
            else:
                cursor.execute("SELECT MA.username_pengguna, MA.nama_tokoh, KJB.nama, A.warna_apparel, A.nama_pekerjaan, A.kategori_apparel, MA.id_koleksi FROM MENGGUNAKAN_APPAREL MA, KOLEKSI_JUAL_BELI KJB, APPAREL A WHERE MA.id_koleksi = KJB.id_koleksi AND KJB.id_koleksi = A.id_koleksi AND MA.username_pengguna = %s", [request.session['username'][0]])
            response['daftar_menggunakan_apparel'] = cursor.fetchall()          
            response['role'] = request.session['username'][2]
        return render(request, 'read_menggunakan_apparel.html', response)
    return redirect('/login')

def create_menggunakan_apparel_1(request):
    if request.session.has_key('username'):
        if request.session['username'][2] == "Pemain":
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT nama FROM TOKOH WHERE username_pengguna = %s", [request.session['username'][0]])
                nama_tokoh = cursor.fetchall()
                result_list_nama = []
                for i in nama_tokoh:
                    result = ''.join(i)
                    result_list_nama.append(result)
                if request.method == "POST":
                    dropdown_nama_tokoh = request.POST['nama_tokoh']
                    return redirect(dropdown_nama_tokoh + '/create')
            return render(request, 'create_menggunakan_apparel_1.html', {'result_list_nama': result_list_nama})
    return redirect('/login')

# role: Pemain
def create_menggunakan_apparel_2(request, nama_tokoh):
    if request.session.has_key('username'):
        if request.session['username'][2] == "Pemain":
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT KT.id_koleksi FROM KOLEKSI_TOKOH KT WHERE KT.username_pengguna = %s AND KT.nama_tokoh = %s AND KT.id_koleksi IN (SELECT A.id_koleksi FROM APPAREL A)", [request.session['username'][0], nama_tokoh])
                apparel = cursor.fetchall()
                result_list_apparel = []
                for i in apparel:
                    result = ''.join(i)
                    result_list_apparel.append(result)
                if request.method == "POST":
                    dropdown_apparel = request.POST['apparel']
                    if check_menggunakan_apparel_availibility(request.session['username'][0], nama_tokoh, dropdown_apparel):
                        cursor.execute("INSERT INTO MENGGUNAKAN_APPAREL VALUES(%s, %s, %s)", [request.session['username'][0], nama_tokoh, dropdown_apparel])
                        return redirect('/menggunakan-apparel')
                    else:
                        return render(request, 'create_menggunakan_apparel_2.html', {'nama_tokoh': nama_tokoh, 'result_list_apparel': result_list_apparel, 'error_message': "Tokoh sedang memakai apparel tersebut. Silakan pilih apparel lain."})
            return render(request, 'create_menggunakan_apparel_2.html', {'nama_tokoh': nama_tokoh, 'result_list_apparel': result_list_apparel, 'error_message': None})
    return redirect('/login')

# role: Pemain
def delete_menggunakan_apparel(request, username, nama, id):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("DELETE FROM MENGGUNAKAN_APPAREL WHERE username_pengguna = %s AND nama_tokoh = %s AND id_koleksi = %s", [username, nama, id])
    return redirect('/menggunakan-apparel')

def check_menggunakan_apparel_availibility(username, name, apparel):
    result = True
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM MENGGUNAKAN_APPAREL WHERE username_pengguna = %s AND nama_tokoh = %s AND id_koleksi = %s", [username, name, apparel])
        level_row = cursor.fetchone()
        if level_row != None:
            result = False
    return result