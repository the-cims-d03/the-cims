from django import forms

class AdminRegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control Font-size-20', 'id': 'username', 'placeholder':'Username'}))
    password = forms.CharField(label='Password', max_length=20, widget=forms.PasswordInput(attrs={'class': 'form-control Font-size-20', 'id': 'password', 'placeholder':'Password'}))

class PemainRegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control Font-size-20', 'id': 'username', 'placeholder':'Username'}))
    email = forms.CharField(label='Email', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control Font-size-20', 'id': 'email', 'placeholder':'Email'}))
    password = forms.CharField(label='Password', max_length=20, widget=forms.PasswordInput(attrs={'class': 'form-control Font-size-20', 'id': 'password', 'placeholder':'Password'}))
    no_hp = forms.CharField(label='No HP', max_length=12, widget=forms.TextInput(attrs={'class': 'form-control Font-size-20', 'id': 'no_hp', 'placeholder':'Nomor Handphone'}))