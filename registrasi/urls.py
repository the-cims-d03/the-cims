from django.urls import path
from .views import *

app_name = 'registrasi'
urlpatterns = [
    path('', register_pilih, name='register_pilih'),
    path('admin', register_admin, name='register_admin'),
    path('pemain', register_pemain, name='register_pemain'),
]