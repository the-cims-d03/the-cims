from django.urls import reverse
from urllib import response
from django.shortcuts import render
from .forms import *
from django.db import connection
from django.shortcuts import redirect
from home.views import *

def register_pilih(request):
    context = {
        'register_admin': reverse('registrasi:register_admin'),
        'register_pemain': reverse('registrasi:register_pemain'),
    }
    return render(request, 'register.html', context)

def check_username_availability(username):
    result = True
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM AKUN WHERE username = %s", [username])
        username_row = cursor.fetchone()
        if username_row != None:
            result = False
    return result

def check_email_availability(email):
    result = True
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM PEMAIN WHERE email = %s", [email])
        email_row = cursor.fetchone()
        if email_row != None:
            result = False
    return result

def register_admin(request):
    response = {}
    if (request.method == "POST"):
        form = AdminRegistrationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            if check_username_availability(username):
                with connection.cursor() as cursor:
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute("INSERT INTO AKUN VALUES(%s)", [username])
                    cursor.execute("INSERT INTO ADMIN VALUES(%s, %s)", [username, password])
                    cursor.execute("SELECT * FROM \
                       (SELECT * FROM PEMAIN UNION SELECT username, Null as email, password, Null as no_hp, Null as koin FROM ADMIN) AS P \
                        WHERE username ='" + username + "' AND PASSWORD = '" + password + "'")
                    result = cursor.fetchone()
                    role = "Admin"
                    cursor.execute("SET SEARCH_PATH TO public")
                    request.session['username'] = [username, password, role, result]
                    return redirect('/homepage')
            else:
                form.add_error('username', "Username ini tidak tersedia")
                response['register_admin_form'] = form
                return render(request, 'register_admin.html', response)
    form = AdminRegistrationForm()
    response["register_admin_form"] = form
    return render(request, "register_admin.html", response)

def register_pemain(request):
    response = {}
    if (request.method == "POST"):
        form = PemainRegistrationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']
            no_hp = request.POST['no_hp']
            if check_username_availability(username) and check_email_availability(email):
                with connection.cursor() as cursor:
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute("INSERT INTO AKUN VALUES(%s)", [username])
                    cursor.execute("INSERT INTO PEMAIN VALUES(%s, %s, %s, %s, %s)", [username, email, password, no_hp, 0])
                    cursor.execute("SELECT * FROM \
                       (SELECT * FROM PEMAIN UNION SELECT username, Null as email, password, Null as no_hp, Null as koin FROM ADMIN) AS P \
                        WHERE username ='" + username + "' AND PASSWORD = '" + password + "'")
                    result = cursor.fetchone()
                    role = "Pemain"
                    cursor.execute("SET SEARCH_PATH TO public")
                    request.session['username'] = [username, password, role, result]
                    return redirect('/homepage')
            else:
                if check_username_availability(username) == False:
                    form.add_error('username', "Username ini tidak tersedia")
                if check_email_availability(email) == False:
                    form.add_error('email', "Email ini tidak tersedia")
                response['register_pemain_form'] = form
                return render(request, 'register_pemain.html', response)

    form = PemainRegistrationForm()
    response["register_pemain_form"] = form
    return render(request, "register_pemain.html", response)

def login(request, username, password):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM \
                       (SELECT * FROM PEMAIN UNION SELECT username, Null as email, password, Null as no_hp, Null as koin FROM ADMIN) AS P \
                        WHERE username ='" + username + "' AND PASSWORD = '" + password + "'")
        result = cursor.fetchone()
        if result[1] == None:
            role = "Admin"
        else:
            role = "Pemain"

        if(result == None): 
            return HttpResponseNotFound("The user does not exist")

        cursor.execute("SET SEARCH_PATH TO public")
        request.session['username'] = [username, password, role, result]
        return redirect('/homepage')