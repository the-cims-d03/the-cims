from django.urls import path
from .views import *

app_name = 'level'
urlpatterns = [
    path('', read_level, name='read_level'),
    path('create', create_level, name='create_level'),
    path('<level>/update', update_level, name='update_level'),
    path('<level>/delete', delete_level, name='delete_level')
]
