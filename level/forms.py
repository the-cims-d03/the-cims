from django import forms

class CreateLevelForm(forms.Form):
    level = forms.IntegerField(label='Level', widget=forms.NumberInput(attrs={'id': 'level', 'placeholder': 'level'}))
    xp = forms.IntegerField(label='XP', widget=forms.NumberInput(attrs={'id': 'xp', 'placeholder': 'xp'}))

class UpdateLevelForm(forms.Form):
    level = forms.IntegerField(label='Level', widget=forms.NumberInput(attrs={'id': 'level', 'placeholder': 'level', 'readonly': True}))
    xp = forms.IntegerField(label='XP', widget=forms.NumberInput(attrs={'id': 'xp', 'placeholder': 'xp'}))