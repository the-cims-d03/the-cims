from urllib import response
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from .forms import *

# role: Admin & Pemain
def read_level(request):
    response = {}
    if request.session.has_key('username'):
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            cursor.execute("SELECT * FROM LEVEL")
            response['daftar_level'] = cursor.fetchall()
            cursor.execute("SELECT * FROM LEVEL L WHERE L.level NOT IN (SELECT T.level FROM TOKOH T)")
            response['daftar_level_deletable'] = cursor.fetchall()
            response['role'] = request.session['username'][2]
        return render(request, 'read_level.html', response)
    return redirect('/login')

# role: Admin
def create_level(request):
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            if (request.method == "POST"):
                form = CreateLevelForm(request.POST)
                if form.is_valid():
                    level = request.POST['level']
                    xp = request.POST['xp']
                    if check_level_availability(level):
                        with connection.cursor() as cursor:
                            cursor.execute("SET SEARCH_PATH TO THECIMS")
                            cursor.execute("INSERT INTO LEVEL VALUES(%s, %s)", [level, xp])
                        return redirect('/level')
                    else:
                        form.add_error('level', "Level ini sudah tercatat")
                        response["level_form"] = form
                        return render(request, "create_level.html", response)
            form = CreateLevelForm()
            response["level_form"] = form
            return render(request, "create_level.html", response)
    return redirect('/login')

# role: Admin
def update_level(request, level):
    data = {}
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            if (request.method == "POST"):
                form = UpdateLevelForm(request.POST)
                if form.is_valid():
                    xp = request.POST['xp']
                    with connection.cursor() as cursor:
                        cursor.execute("SET SEARCH_PATH TO THECIMS")
                        cursor.execute("UPDATE LEVEL SET xp = %s WHERE level = %s", [xp, level])
                        return redirect('/level')

            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM LEVEL WHERE level = %s", [level])
                level_data = cursor.fetchone()

            data['level'] = level_data[0]
            data['xp'] = level_data[1]
            form = UpdateLevelForm(data)
            response['level_form'] = form
            response['level'] = level

            return render(request, 'update_level.html', response)
    return redirect('/login')

# role: Admin
def delete_level(request, level):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("DELETE FROM LEVEL WHERE level = %s", [level])
    return redirect('/level')

def check_level_availability(level):
    result = True
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO THECIMS")
        cursor.execute("SELECT * FROM LEVEL WHERE level = %s", [level])
        level_row = cursor.fetchone()
        if level_row != None:
            result = False
    return result
