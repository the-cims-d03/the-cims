from django import forms

class BekerjaForm(forms.Form):
    nama_tokoh = forms.CharField(label='nama_tokoh', max_length=50)
    nama_pekerjaan = forms.CharField(label='nama_pekerjaan', max_length=20)
    base_salary = forms.IntegerField(label="base_salary")

class PekerjaanForm(forms.Form):
    nama = forms.CharField(label='nama',max_length=20)
    base_honor = forms.IntegerField(label='base_honor')
  