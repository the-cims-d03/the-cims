from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.db import connection
from .forms import *
from datetime import datetime
# Create your views here.

def fetch_to_str(cursor):
    desc = cursor.fetchall()
    nt_result = tuple([col[0] for col in desc])
    return nt_result

def index(request):
    cursor = connection.cursor()
    
    result = []
    try:
        if request.session.has_key('username'):
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            role = request.session['username'][2]
            cursor.execute("SELECT * FROM PEKERJAAN")
            result = cursor.fetchall()
            
            cursor.execute('''
                SELECT nama FROM PEKERJAAN
                WHERE nama NOT IN (SELECT pekerjaan FROM TOKOH WHERE pekerjaan IS NOT NULL)
                AND nama NOT IN (SELECT nama_pekerjaan FROM BEKERJA WHERE nama_pekerjaan IS NOT NULL)
                AND nama NOT IN (SELECT nama_pekerjaan FROM APPAREL WHERE nama_pekerjaan IS NOT NULL);
            ''')
            list_nama = fetch_to_str(cursor)
            print(type(result))
            print(list_nama)
            new_result = []
            for res in result:
                new_res = list(res)
                if res[0] in list_nama:
                    new_res.append(True)
                else:
                    new_res.append(False)
                new_result.append(new_res)
            print(new_result)
            return render(request, 'read_pekerjaan.html',{'result' : new_result})
        else:
            return HttpResponseRedirect("/login")
    except Exception as e:
        print(e)
    finally:
        cursor.close()

def create_pekerjaan(request):
    cursor = connection.cursor()
    try:
        if request.session.has_key('username') and request.session['username'][2] == 'Admin':
            if request.method == "POST":
                form = PekerjaanForm(request.POST)
                print(request.POST)
                print(form.errors)
                if form.is_valid():
                    cursor.execute("SET search_path to THECIMS");
                    cursor.execute(f'''
                        INSERT INTO PEKERJAAN VALUES
                        ('{form.cleaned_data['nama']}', {form.cleaned_data['base_honor']})
                    ''')
                    return HttpResponseRedirect('/pekerjaan')
            return render(request,"create_pekerjaan.html",{})
        else:
            return HttpResponseRedirect('/pekerjaan')
    except Exception as e:
        print(e)
    finally:
        cursor.close()

def update_pekerjaan(request,nama):
    cursor = connection.cursor()
    try:
        if request.session.has_key('username') and request.session['username'][2] == 'Admin':
            if request.method == "POST" :
                form = PekerjaanForm(request.POST)
                print(request.POST)
                print(form.errors)
                if form.is_valid():
                    cursor.execute("SET search_path to THECIMS");
                    cursor.execute(f'''
                        UPDATE PEKERJAAN 
                        SET base_honor = {form.cleaned_data['base_honor']}
                        WHERE nama = '{form.cleaned_data['nama']}';
                    ''')
                    return HttpResponseRedirect('/pekerjaan')
            else:
                return render(request,"update_pekerjaan.html",{'nama' :nama})
        else:
            return HttpResponseRedirect('/pekerjaan')
    except Exception as e:
        print(e)
    finally:
        cursor.close()


def delete_pekerjaan(request,nama):
    cursor = connection.cursor()
    try:
        if request.session.has_key('username'):
            print("halo")
            print(request.session['username'])
            if request.method == "POST" and request.session['username'][2] == 'Admin':
                print("masuk post admin")
                cursor.execute("SET search_path to THECIMS");
                cursor.execute(f'''
                        DELETE FROM PEKERJAAN 
                        WHERE nama = '{nama}'
                    ''')
                return HttpResponseRedirect('/pekerjaan')
    except Exception as e:
        print(e)
    finally:
        cursor.close()
def list_bekerja(request):
    cursor = connection.cursor()
    result = []
    try:
        if request.session.has_key('username'):
            role = request.session['username'][2]
            nama = request.session['username'][0]
            cursor.execute("SET SEARCH_PATH TO THECIMS")
            if role == "Pemain":
                cursor.execute(f'''
                SELECT nama_tokoh,nama_pekerjaan,timestamp,keberangkatan_ke,honor 
                FROM BEKERJA WHERE username_pengguna = '{nama}'
                ''')
            else:
                cursor.execute("SELECT username_pengguna,nama_tokoh,nama_pekerjaan,timestamp,keberangkatan_ke,honor FROM BEKERJA")
            result = cursor.fetchall()
            return render(request, 'read_bekerja.html',{'result' : result})
    except Exception as e:
        print(e)
    finally:
        cursor.close()

def create_bekerja(request):
    cursor = connection.cursor()
    result = []
    try:
        if request.session.has_key('username'):
            if request.method == "POST":
                form = BekerjaForm(request.POST)
                username_pengguna = request.session['username'][0]
                print("HMM")
                print(form.errors)
                if form.is_valid():
                    print("YES")
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute(f'''
                        INSERT INTO BEKERJA VALUES
                        (
                            '{username_pengguna}',
                            '{form.cleaned_data['nama_tokoh']}',
                            '{datetime.now()}',
                            '{form.cleaned_data['nama_pekerjaan']}',
                            update_keberangkatan('{username_pengguna}','{form.cleaned_data['nama_tokoh']}' )
                            );
                    ''')
                    return HttpResponseRedirect("/pekerjaan/bekerja")
            else:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(f'''
                    SELECT  DISTINCT nama_tokoh,nama_pekerjaan,p.base_honor
                    FROM BEKERJA b,PEKERJAAN p
                    WHERE b.nama_pekerjaan = p.nama
                    AND b.username_pengguna = '{request.session["username"][0]}'
                ''')
                result = cursor.fetchall()
                return render(request, 'create_bekerja.html',{'result' : result})
    except Exception as e:
        print(e)
    finally:
        cursor.close()