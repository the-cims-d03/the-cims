from django.urls import path

from .views import *

app_name = 'pekerjaan'

urlpatterns = [
    path('', index, name='index'),
    path('create',create_pekerjaan,name='create_pekerjaan'),
    path('bekerja',list_bekerja,name="read_bekerja"),
    path('bekerja/create',create_bekerja,name="create_bekerja"),
    path('<str:nama>/update',update_pekerjaan,name="update_pekerjaan"),
    path('<str:nama>/delete',delete_pekerjaan,name='delete_pekerjaan'),
    
]