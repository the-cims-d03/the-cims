from datetime import datetime
from urllib import response
from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone


def read_makan(request):
    if request.session.has_key('username'):
        if request.session['username'][2] == "Admin":
            response = {}
            response['role'] = [request.session['username'][2]]
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM MAKAN")
                response['daftar_makan'] = cursor.fetchall()
            return render(request, 'read_makan.html', response)
        elif request.session['username'][2] == "Pemain":
            username = request.session['username'][0]
            response = {}
            response['role'] = request.session['username'][2]
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM MAKAN WHERE username_pengguna = %s", [username])
                response['daftar_makan'] = cursor.fetchall()
            return render(request, 'read_makan.html', response)
    else:
        return HttpResponseRedirect('/login')

# Pemain Only
@csrf_exempt
def create_makan(request):
    response = {}
    if request.session.has_key('username'):
        if request.session['username'][2] == "Pemain":
            username = request.session['username'][0]
            if (request.method == "POST"):
                body = request.POST
                nama_tokoh = body.get('nama_tokoh', '')
                nama_makanan = body.get('nama_makanan', '')

                if not body or not (nama_tokoh and nama_makanan):
                    return HttpResponse("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu", status=400)

                times = timezone.now()
                with connection.cursor() as cursor:
                    cursor.execute("SET SEARCH_PATH TO THECIMS")
                    cursor.execute("INSERT INTO MAKAN VALUES(%s, %s, %s, %s)", [
                        username, nama_tokoh, times , nama_makanan])
                    return redirect('/makan/')
                
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute(f"""
                    SELECT nama FROM TOKOH
                    WHERE username_pengguna = '{username}'
                """)
                response['daftar_nama_tokoh'] = cursor.fetchall()

            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO THECIMS")
                cursor.execute("SELECT * FROM MAKANAN")
                response['daftar_makanan'] = cursor.fetchall()

            daftar_nama_tokoh = [('Pilih tokoh', ''), ] + \
                response['daftar_nama_tokoh']
            daftar_makanan = [
                ('Pilih makanan', ''), ] + response['daftar_makanan']

            response['daftar_nama_tokoh'] = daftar_nama_tokoh
            response['daftar_makanan'] = daftar_makanan

            return render(request, 'create_makan.html', response)
    else:
        return HttpResponseRedirect('/login')
